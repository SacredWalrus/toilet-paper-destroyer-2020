﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class WorldController : MonoBehaviour
{
    public enum Country {Russia, Italy, USA, Market};
    public Country curr_county;

    public GameObject back_russia, back_italy, back_usa, back_market;

    public TMP_Text t_county_name;

    private int chose_country;  //1 - Market, 2 - Russia, 3 - Italy, 4 = USA

    public GameObject but_subs, but_chose, t_active;


    private void Start()
    {
        chose_country = 1;
        t_active.SetActive(false);

        if (!PlayerPrefs.HasKey("country"))
        {
            chose_country = 1;
            PlayerPrefs.SetInt("country", 1);
        }
        else
        {
            chose_country = PlayerPrefs.GetInt("country");
            ExitScreen();
        }
    }

    private void Update()
    {
        if (LevelController.subs_active)
        {
            but_subs.SetActive(false);

            if (curr_county == Country.Market)
            {
                if (chose_country == 1)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(false);
                    t_active.SetActive(true);
                }

                if (chose_country != 1)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(true);
                    t_active.SetActive(false);
                }
            }

            if (curr_county == Country.Russia)
            {
                if (chose_country == 2)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(false);
                    t_active.SetActive(true);
                }

                if (chose_country != 2)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(true);
                    t_active.SetActive(false);
                }
            }

            if (curr_county == Country.Italy)
            {
                if (chose_country == 3)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(false);
                    t_active.SetActive(true);
                }

                if (chose_country != 3)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(true);
                    t_active.SetActive(false);
                }
            }

            if (curr_county == Country.USA)
            {
                if (chose_country == 4)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(false);
                    t_active.SetActive(true);
                }

                if (chose_country != 4)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(true);
                    t_active.SetActive(false);
                }
            }
        }

        if (!LevelController.subs_active)
        {
            if (curr_county == Country.Market)
            {
                if (chose_country == 1)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(false);
                    t_active.SetActive(true);
                }

                if (chose_country != 1)
                {
                    but_subs.SetActive(false);
                    but_chose.SetActive(true);
                    t_active.SetActive(false);
                }
            }

            if (curr_county != Country.Market)
            {
                but_subs.SetActive(true);
                but_chose.SetActive(false);
                t_active.SetActive(false);
            }
        }
    }

    public void ChoseCountry()
    {
        if (curr_county == Country.Market)
        {
            chose_country = 1;
            PlayerPrefs.SetInt("country", 1);
        }
        if (curr_county == Country.Russia)
        {
            chose_country = 2;
            PlayerPrefs.SetInt("country", 2);
        }
        if (curr_county == Country.Italy)
        {
            chose_country = 3;
            PlayerPrefs.SetInt("country", 3);
        }
        if (curr_county == Country.USA)
        {
            chose_country = 4;
            PlayerPrefs.SetInt("country", 4);
        }
    }

    public void Russia()
    {
        back_italy.SetActive(false);
        back_russia.SetActive(true);
        back_market.SetActive(false);
        back_usa.SetActive(false);

        curr_county = Country.Russia;

        if (Localization.curr_language == "ru")
            t_county_name.text = "РОССИЯ";
        if (Localization.curr_language == "eng")
            t_county_name.text = "RUSSIA";
    }

    public void USA()
    {
        back_italy.SetActive(false);
        back_russia.SetActive(false);
        back_usa.SetActive(true);
        back_market.SetActive(false);

        curr_county = Country.USA;

        if (Localization.curr_language == "ru")
            t_county_name.text = "АМЕРИКА";
        if (Localization.curr_language == "eng")
            t_county_name.text = "USA";
    }

    public void Italy()
    {
        back_italy.SetActive(true);
        back_russia.SetActive(false);
        back_usa.SetActive(false);
        back_market.SetActive(false);

        curr_county = Country.Italy;

        if (Localization.curr_language == "ru")
            t_county_name.text = "ИТАЛИЯ";
        if (Localization.curr_language == "eng")
            t_county_name.text = "ITALY";
    }

    public void Market()
    {
        back_italy.SetActive(false);
        back_russia.SetActive(false);
        back_usa.SetActive(false);
        back_market.SetActive(true);

        curr_county = Country.Market;

        if (Localization.curr_language == "ru")
            t_county_name.text = "РЫНОК";
        if (Localization.curr_language == "eng")
            t_county_name.text = "MARKET";
    }

    public void ExitScreen()
    {
        if (chose_country == 1)
        {
            Market();
        }
        if (chose_country == 2)
        {
            Russia();
        }
        if (chose_country == 3)
        {
            Italy();
        }
        if (chose_country == 4)
        {
            USA();
        }
    }

    void Save()
    {

    }

    void Load()
    {

    }
}
