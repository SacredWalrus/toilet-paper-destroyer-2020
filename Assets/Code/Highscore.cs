﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Highscore : MonoBehaviour
{
    public TMP_Text t_highscore;
    public static int score;

    void Start()
	{
        if (!PlayerPrefs.HasKey("highscore"))
        {
            PlayerPrefs.SetInt("highscore", 0);
            score = 0;
        }
        else score = PlayerPrefs.GetInt("highscore");
    }

    private void Update()
    {
        t_highscore.text = "" + score;
    }

    public void Check_Score(int s)
    {
        if(s > score)
        {
            score = s;
            PlayerPrefs.SetInt("highscore", score);
        }
    }
}
