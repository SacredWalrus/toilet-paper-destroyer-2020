﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NotificationSamples;
using System;

public class NotificationController : MonoBehaviour
{
    [SerializeField] private GameNotificationsManager notificationManager; 


    private void Start()
    {
        InitializeNotification();
        CreateNotification("Туалетная бумага атакует!", "Возвращайся в игру и останови вторжение туалетной бумаги!", DateTime.Now.AddHours(16));
    }

    private void InitializeNotification()
    {
        GameNotificationChannel channel = new GameNotificationChannel("android_not", "Zagolovok", "Opisanie");
        notificationManager.Initialize(channel);
    }

    private void CreateNotification(string title, string body, DateTime time)
    {
        IGameNotification notification = notificationManager.CreateNotification();
        if(notification != null)
        {
            notification.Title = title;
            notification.Body = body;
            notification.DeliveryTime = time;
            notificationManager.ScheduleNotification(notification);
        }
    }
}
