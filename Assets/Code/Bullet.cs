﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float bullet_speed;
    public float bullet_dmg;

    private void Update()
    {
        transform.Translate(Vector2.up * bullet_speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "DeadZone")
        {
            Destroy(gameObject);
        }
    }
}
