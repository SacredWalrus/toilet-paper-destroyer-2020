﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DaysReward : MonoBehaviour
{

    private void Start()
    {
        DateTime date = new DateTime();
        date = DateTime.Now;

        if (LevelController.subs_active)
        {
            if (PlayerPrefs.GetInt("day") != date.Day)
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().heart += 5;
                GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 10;
                PlayerPrefs.SetInt("day", date.Day);
            }
        }
    }
}
