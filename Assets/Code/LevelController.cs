﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelController : MonoBehaviour
{
    public TMP_Text t_win;

    public static bool subs_active;  //true - куплена

    #region Объекты для анимаций @Menu@
    public GameObject pause_but;
    public GameObject through_bars;
    public GameObject down_panel;
    public GameObject centre_panel;
    public GameObject t_tap_to_play;

    public bool anim_enter_menu, anim_exit_menu;
    #endregion

    #region Объекты для анимаций @Game@
    public GameObject progress_bar_obj;
    public GameObject pause_but_obj;

    public bool anim_enter_game, anim_exit_game;
    #endregion

    #region Объекты для анимаций @World@
    //public GameObject progress_bar_obj;

    public bool anim_enter_world, anim_exit_world;
    #endregion

    //Слои
    public List<int> layer_mass = new List<int>();

    //Экономика
    public float money;  //Монеты
    public float ruby;  //Рубины
    public float heart;  //Сердца
    public float earned_money;  //Заработанные на уровне деньги
    public TMP_Text t_money_count, t_ruby_count, t_heart_count;

    //Уровень
    public int curr_level;
    public TMP_Text t_level;
    public bool move_accept;  //Можно ли сейчас двигаться и стрелять

    //Враги
    public List<GameObject> big_crn;
    public List<GameObject> medium_crn;
    public List<GameObject> small_crn;

    public List<GameObject> active_enemy = new List<GameObject>();
    private bool last_check;
    public bool dont_spawn;

    //Очки
    public TMP_Text t_score;
    public float score;
    public float new_score;
    public bool win;

    //Прогресс-бар
    public Image progress_bar;
    public float curr_enemy;  //Переменная которая стремится к new переменной
    public float new_curr_enemy;  //Обновленная переменная
    public int max_enemy;

    //Игровые окна
    public GameObject pn_menu, pn_game, pn_world;

    //Поп-апы
    public GameObject popUp_win;

    public float timer_to_spawn;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("subs_active"))
            subs_active = false;
        else PlayerPrefs.SetInt("subs_active", 0);

        if (PlayerPrefs.GetInt("subs_active") == 1)
            subs_active = true;

        if (PlayerPrefs.GetInt("subs_active") == 0)
            subs_active = false;

        anim_enter_menu = anim_exit_menu = false;
        AnimationsControllerMenu();

        dont_spawn = false;
        win = false;

        if (!PlayerPrefs.HasKey("money"))
            money = 0;
        else money = PlayerPrefs.GetFloat("money");

        if (!PlayerPrefs.HasKey("ruby"))
            ruby = 5;
        else ruby = PlayerPrefs.GetFloat("ruby");

        if (!PlayerPrefs.HasKey("heart"))
            heart = 2;
        else ruby = PlayerPrefs.GetFloat("heart");

        if (!PlayerPrefs.HasKey("curr_level"))
            curr_level = 1;
        else curr_level = PlayerPrefs.GetInt("curr_level");

        pn_game.SetActive(false);
        pn_menu.SetActive(true);
        move_accept = false;
        last_check = false;
    }

    private void Update()
    {
        t_money_count.text = "" + money;
        t_ruby_count.text = "" + ruby;
        t_heart_count.text = "" + heart;

        t_score.text = (int)score + "";
        if (Localization.curr_language == "eng")
            t_level.text = "Level " + curr_level;

        if (Localization.curr_language == "ru")
            t_level.text = "Уровень " + curr_level;

        TextScore_Anim();
        ProgressBarController();

        if (new_curr_enemy >= max_enemy && !last_check && move_accept && !win)
        {
            if (active_enemy.Count == 0)
            {
                EndGame();
                curr_level++;
                PlayerPrefs.SetInt("curr_level", curr_level);
                move_accept = false;
                dont_spawn = true;
                gameObject.GetComponent<Highscore>().Check_Score((int)score);

                if (Localization.curr_language == "ru")
                    t_win.text = "Победа!";
                if (Localization.curr_language == "eng")
                    t_win.text = "Win!";
            }

            if (active_enemy.Count > 0)
            {
                max_enemy += active_enemy.Count;
                last_check = true;
                dont_spawn = true;
            }
        }

        if (last_check)
        {
            if (active_enemy.Count == 0)
            {
                EndGame();
                curr_level++;
                PlayerPrefs.SetInt("curr_level", curr_level);
                move_accept = false;
                last_check = false;
                dont_spawn = true;
                gameObject.GetComponent<Highscore>().Check_Score((int)score);

                if (Localization.curr_language == "ru")
                    t_win.text = "Победа!";
                if (Localization.curr_language == "eng")
                    t_win.text = "Win!";
            }
        }
    }

    IEnumerator Starting()
    {
        yield return new WaitForSeconds(timer_to_spawn);
        if (!win && !dont_spawn)
        {
            if (active_enemy == null || active_enemy.Count <= 6)
            {
                int rand = Random.Range(1, 4);

                switch (rand)
                {
                    case 1:
                        GameObject obj_big = Instantiate(big_crn[Random.Range(0, big_crn.Count)], new Vector2(Random.Range(-8.1f, 8.1f), 42.05f), transform.rotation) as GameObject;
                    layer_rand_1:

                        int layer_r_1 = Random.Range(0, 950);
                        if (layer_mass != null)
                        {
                            if (layer_mass.Contains(layer_r_1))
                            {
                                goto layer_rand_1;
                            }
                        }

                        obj_big.transform.position = new Vector3(obj_big.transform.position.x, obj_big.transform.position.y, layer_r_1);

                        layer_mass.Add(layer_r_1);

                        active_enemy.Add(obj_big);
                        break;
                    case 2:
                        GameObject obj_medium = Instantiate(medium_crn[Random.Range(0, medium_crn.Count)], new Vector2(Random.Range(-8.1f, 8.1f), 40f), transform.rotation) as GameObject;
                    layer_rand_2:

                        int layer_r_2 = Random.Range(0, 950);
                        if (layer_mass != null)
                        {
                            if (layer_mass.Contains(layer_r_2))
                            {
                                goto layer_rand_2;
                            }
                        }

                        obj_medium.transform.position = new Vector3(obj_medium.transform.position.x, obj_medium.transform.position.y, layer_r_2);

                        layer_mass.Add(layer_r_2);

                        active_enemy.Add(obj_medium);
                        break;
                    case 3:
                        GameObject obj_small = Instantiate(small_crn[Random.Range(0, small_crn.Count)], new Vector2(Random.Range(-8.1f, 8.1f), 38f), transform.rotation) as GameObject;
                    layer_rand_3:

                        int layer_r_3 = Random.Range(0, 950);
                        if (layer_mass != null)
                        {
                            if (layer_mass.Contains(layer_r_3))
                            {
                                goto layer_rand_3;
                            }
                        }

                        obj_small.transform.position = new Vector3(obj_small.transform.position.x, obj_small.transform.position.y, layer_r_3);

                        layer_mass.Add(layer_r_3);

                        active_enemy.Add(obj_small);
                        break;
                }
            }

            StartCoroutine(Starting());
        }
    }

    IEnumerator INumStartGame()
    {
        anim_exit_menu = true;
        anim_enter_menu = false;

        anim_enter_game = true;
        anim_exit_game = false;

        AnimationsControllerMenu();
        win = false;
        last_check = false;
        dont_spawn = false;
        

        yield return new WaitForSeconds(0.5f);
        AnimationsControllerGame();
        StartCoroutine(Starting());
        move_accept = true;
        GameObject.Find("Player").GetComponent<PlayerController>().StartShoot();
        GameObject.Find("Player").GetComponent<PlayerController>().press = true;
        score = 0;
        curr_enemy = 0;
        max_enemy = 50;
        pn_menu.SetActive(false);
        pn_game.SetActive(true);
    }

    public void StartGame()
    {
        StartCoroutine(INumStartGame());
    }

    public void ReturnGame()
    {
        StartCoroutine(Starting());
    }

    public void AnimationsControllerMenu()
    {
        if (!anim_enter_menu && !anim_exit_menu)
        {
            pause_but.GetComponent<Animator>().SetBool("enter", false);
            pause_but.GetComponent<Animator>().SetBool("exit", false);

            through_bars.GetComponent<Animator>().SetBool("enter", false);
            through_bars.GetComponent<Animator>().SetBool("exit", false);

            down_panel.GetComponent<Animator>().SetBool("enter", false);
            down_panel.GetComponent<Animator>().SetBool("exit", false);

            centre_panel.GetComponent<Animator>().SetBool("enter", false);
            centre_panel.GetComponent<Animator>().SetBool("exit", false);

            t_tap_to_play.GetComponent<Animator>().SetBool("enter", false);
            t_tap_to_play.GetComponent<Animator>().SetBool("exit", false);
        }

        if (anim_enter_menu && !anim_exit_menu)
        {
            pause_but.GetComponent<Animator>().SetBool("enter", true);
            pause_but.GetComponent<Animator>().SetBool("exit", false);

            through_bars.GetComponent<Animator>().SetBool("enter", true);
            through_bars.GetComponent<Animator>().SetBool("exit", false);

            down_panel.GetComponent<Animator>().SetBool("enter", true);
            down_panel.GetComponent<Animator>().SetBool("exit", false);

            centre_panel.GetComponent<Animator>().SetBool("enter", true);
            centre_panel.GetComponent<Animator>().SetBool("exit", false);

            t_tap_to_play.GetComponent<Animator>().SetBool("enter", true);
            t_tap_to_play.GetComponent<Animator>().SetBool("exit", false);
        }

        if (anim_exit_menu && !anim_enter_menu)
        {
            pause_but.GetComponent<Animator>().SetBool("enter", false);
            pause_but.GetComponent<Animator>().SetBool("exit", true);

            through_bars.GetComponent<Animator>().SetBool("enter", false);
            through_bars.GetComponent<Animator>().SetBool("exit", true);

            down_panel.GetComponent<Animator>().SetBool("enter", false);
            down_panel.GetComponent<Animator>().SetBool("exit", true);

            centre_panel.GetComponent<Animator>().SetBool("enter", false);
            centre_panel.GetComponent<Animator>().SetBool("exit", true);

            t_tap_to_play.GetComponent<Animator>().SetBool("enter", false);
            t_tap_to_play.GetComponent<Animator>().SetBool("exit", true);
        }
    }

    public void AnimationsControllerGame()
    {
        if (anim_enter_game && !anim_exit_game)
        {
            progress_bar_obj.GetComponent<Animator>().SetBool("exit", false);
            progress_bar_obj.GetComponent<Animator>().SetBool("enter", true);

            pause_but_obj.GetComponent<Animator>().SetBool("exit", false);
            pause_but_obj.GetComponent<Animator>().SetBool("enter", true);
        }

        if (anim_exit_game && !anim_enter_game)
        {
            progress_bar_obj.GetComponent<Animator>().SetBool("exit", true);
            progress_bar_obj.GetComponent<Animator>().SetBool("enter", false);

            pause_but_obj.GetComponent<Animator>().SetBool("exit", true);
            pause_but_obj.GetComponent<Animator>().SetBool("enter", false);
        }
    }

    private void TextScore_Anim()
    {
        if (score < new_score)
        {
            score += Time.deltaTime * 100;
        }

        if (score > new_score)
        {
            score = new_score;
        }
    }

    private void ProgressBarController()
    {
        progress_bar.fillAmount = curr_enemy / max_enemy;

        if (curr_enemy < new_curr_enemy) curr_enemy += Time.deltaTime * 10;
    }

    private void EndGame()
    {
        win = true;
        GameObject.Find("GameManager").GetComponent<Upgrader>().CoronavirusCountController();
        foreach (GameObject obj in active_enemy)
        {
            Destroy(obj);
        }

        active_enemy.Clear();
        layer_mass.Clear();
        popUp_win.SetActive(true);
    }

    IEnumerator INumOpenWorldPanel()
    {
        anim_exit_menu = true;
        anim_enter_menu = false;

        AnimationsControllerMenu();

        yield return new WaitForSeconds(0.5f);

        pn_world.GetComponent<Animator>().SetBool("exit", false);
        pn_menu.SetActive(false);
        pn_world.SetActive(true);
    }

    IEnumerator INumClosedWorldPanel()
    {
        pn_world.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(0.5f);

        pn_menu.SetActive(true);
        pn_world.SetActive(false);

        anim_exit_menu = false;
        anim_enter_menu = true;

        AnimationsControllerMenu();
    }

    public void OpenWorldPanel()
    {
        StartCoroutine(INumOpenWorldPanel());
    }

    public void CloseWorldPanel()
    {
        StartCoroutine(INumClosedWorldPanel());
    }
}