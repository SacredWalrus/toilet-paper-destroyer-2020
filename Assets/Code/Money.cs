﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Money : MonoBehaviour
{
    GameObject tg;
    Rigidbody2D rb2d;

    private float Xdif, Ydif;
    public float speed;
    private Vector2 target_dir;
    private Vector3 target_this;

    private void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        tg = GameObject.Find("Player");
        //transform.DOMove(tg.transform.position, 0.75f);
    }

    private void Update()
    {
        tg = GameObject.Find("Player");

        if (transform.position.x <= tg.transform.position.x + 2 &&
            transform.position.x >= tg.transform.position.x - 2 &&
            transform.position.y >= tg.transform.position.y - 2 &&
            transform.position.y <= tg.transform.position.y + 2) 
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().earned_money += GameObject.Find("GameManager").GetComponent<Upgrader>().money_count;
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        Xdif = tg.transform.position.x - transform.position.x;
        Ydif = tg.transform.position.y - transform.position.y;
        target_dir = new Vector2(Xdif, Ydif);

        rb2d.AddForce(target_dir.normalized * speed);
    }
}
