﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Monetization;
using TMPro;

public class PopUp_Revive : MonoBehaviour
{
    public Image timer_bar;
    public GameObject popUp_win;

    private bool revive;

    public TMP_Text t_win;

    private void Start()
    {
        if (Monetization.isSupported) Monetization.Initialize("3513712", false);

        revive = false;
    }

    public void But_Revive_Ads()
    {
        if (Monetization.IsReady("rewardedVideo"))
        {
            ShowAdCallbacks options = new ShowAdCallbacks();
            options.finishCallback = Revive_Ads_Reward;
            ShowAdPlacementContent ad = Monetization.GetPlacementContent("rewardedVideo") as ShowAdPlacementContent;
            ad.Show(options);
            Time.timeScale = 0;
        }
    }

    private void Revive_Ads_Reward(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            Time.timeScale = 1;
            GameObject.Find("Player").GetComponent<PlayerController>().NotDeathTimer();
            revive = true;
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 2;
            GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = true;
            GameObject.Find("GameManager").GetComponent<LevelController>().win = false;

            foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
            {
                obj.GetComponent<CoronavirusController>().move_accept = true;
            }
            StartCoroutine(ClosePopUpNotFinishGame());
            GameObject.Find("Player").GetComponent<PlayerController>().StartShoot();
            GameObject.Find("GameManager").GetComponent<LevelController>().ReturnGame();
            gameObject.GetComponent<Animator>().SetBool("exit", true);
        }
        else if (result == ShowResult.Skipped)
        {
            //
        }
        else if (result == ShowResult.Failed)
        {
            //
        }
    }

    public void But_Revive_Heart()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().heart >= 1)
        {
            GameObject.Find("Player").GetComponent<PlayerController>().NotDeathTimer();
            revive = true;
            GameObject.Find("GameManager").GetComponent<LevelController>().heart -= 1;
            GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = true;
            GameObject.Find("GameManager").GetComponent<LevelController>().win = false;

            foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
            {
                obj.GetComponent<CoronavirusController>().move_accept = true;
            }
            StartCoroutine(ClosePopUpNotFinishGame());
            GameObject.Find("Player").GetComponent<PlayerController>().StartShoot();
            GameObject.Find("GameManager").GetComponent<LevelController>().ReturnGame();
            gameObject.GetComponent<Animator>().SetBool("exit", true);
        }
    }

    public void StartPopUp()
    {
        revive = false;
        gameObject.GetComponent<Animator>().SetBool("exit", false);
        timer_bar.fillAmount = 1;

        GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = false;

        GameObject.Find("GameManager").GetComponent<LevelController>().win = true;

        foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
        {
            obj.GetComponent<CoronavirusController>().move_accept = false;
        }
    }

    private void Update()
    {
        if (!revive)
            Timer();
    }

    void Timer()
    {
        timer_bar.fillAmount -= Time.deltaTime / 3.5f;

        if (timer_bar.fillAmount <= 0)
        {
            StartCoroutine(ClosePopUp());
        }
    }

    IEnumerator ClosePopUp()
    {
        gameObject.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(0.5f);

        foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
        {
            Instantiate(obj.GetComponent<CoronavirusController>().boom, obj.transform.position, transform.rotation);
            Destroy(obj);
        }

        GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Clear();

        popUp_win.SetActive(true);

        if (Localization.curr_language == "ru")
            t_win.text = "Проигрыш";
        if (Localization.curr_language == "eng")
            t_win.text = "Losing";

        gameObject.SetActive(false);
    }

    IEnumerator ClosePopUpNotFinishGame()
    {
        yield return new WaitForSeconds(0.5f);
        gameObject.SetActive(false);
    }
}
