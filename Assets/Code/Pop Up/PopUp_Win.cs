﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Monetization;

public class PopUp_Win : MonoBehaviour
{
    //Игровые окна
    public GameObject pn_menu, pn_game;
    public TMP_Text t_money;


    private void Start()
    {
        gameObject.GetComponentInChildren<Animator>().SetBool("exit", false);

        if (LevelController.subs_active) GameObject.Find("GameManager").GetComponent<LevelController>().earned_money *= 5;
    }

    private void Update()
    {
        t_money.text = GameObject.Find("GameManager").GetComponent<LevelController>().earned_money + "";
    }

    public void But_Ok()
    {
        gameObject.GetComponentInChildren<Animator>().SetBool("exit", true);
        
        GameObject.Find("GameManager").GetComponent<LevelController>().money += GameObject.Find("GameManager").GetComponent<LevelController>().earned_money;
        PlayerPrefs.SetFloat("money", GameObject.Find("GameManager").GetComponent<LevelController>().money);
        StartCoroutine(EndGame());
    }

    public void But_Money_X2_Ads()
    {
        if (Monetization.IsReady("rewardedVideo"))
        {
            ShowAdCallbacks options = new ShowAdCallbacks();
            options.finishCallback = Money_Ads_Reward;
            ShowAdPlacementContent ad = Monetization.GetPlacementContent("rewardedVideo") as ShowAdPlacementContent;
            ad.Show(options);
        }
    }

    private void Money_Ads_Reward(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().earned_money *= 2;
        }
        else if (result == ShowResult.Skipped)
        {
            //
        }
        else if (result == ShowResult.Failed)
        {
            //
        }
    }

    IEnumerator EndGame()
    {
        GameObject.Find("GameManager").GetComponent<LevelController>().anim_exit_game = true;
        GameObject.Find("GameManager").GetComponent<LevelController>().anim_enter_game = false;

        GameObject.Find("GameManager").GetComponent<LevelController>().anim_exit_menu = false;
        GameObject.Find("GameManager").GetComponent<LevelController>().anim_enter_menu = true;
        GameObject.Find("GameManager").GetComponent<LevelController>().AnimationsControllerGame();
        

        yield return new WaitForSeconds(0.5f);
        pn_game.SetActive(false);
        pn_menu.SetActive(true);
        GameObject.Find("GameManager").GetComponent<LevelController>().AnimationsControllerMenu();

        
        GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = false;
        GameObject.Find("GameManager").GetComponent<LevelController>().score = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().curr_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().new_curr_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().max_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().new_score = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().earned_money = 0;
        gameObject.SetActive(false);
    }
}
