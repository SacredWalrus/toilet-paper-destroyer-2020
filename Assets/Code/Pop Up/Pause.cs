﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    //Игровые окна
    public GameObject pn_menu, pn_game, pn_pause, through_bars;


    public void PauseOn()
    {
        pn_pause.SetActive(true);
        Time.timeScale = 0;
    }

    public void PauseOff()
    {
        Time.timeScale = 1;
        pn_pause.SetActive(false);
    }

    public void Exit_To_Menu()
    {
        Time.timeScale = 1;
        
        foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
        {
            Destroy(obj);
        }

        GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Clear();

        pn_game.SetActive(false);
        pn_menu.SetActive(true);

        GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = false;
        GameObject.Find("GameManager").GetComponent<LevelController>().score = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().curr_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().new_curr_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().max_enemy = 0;
        GameObject.Find("GameManager").GetComponent<LevelController>().new_score = 0;

        GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = false;

        GameObject.Find("GameManager").GetComponent<LevelController>().win = true;

        foreach (GameObject obj in GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy)
        {
            obj.GetComponent<CoronavirusController>().move_accept = false;
        }
        through_bars.GetComponent<Animator>().SetBool("enter", true);
        through_bars.GetComponent<Animator>().SetBool("exit", false);
        pn_pause.SetActive(false);
    }
}
