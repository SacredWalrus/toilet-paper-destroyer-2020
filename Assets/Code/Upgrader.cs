﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Upgrader : MonoBehaviour
{
    //Stats
    public float speed_count;
    public float fire_speed_count;
    public float damage_count;
    public float money_count;
    public float coronavirus_count;

    //Other
    public GameObject panel_speed, panel_fire_speed, panel_damage, panel_money;
    public GameObject buts_chose;
    public GameObject but_exit;
    public Image but_speed, but_fire_speed, but_damage, but_money;
    public Sprite but_speed_active, but_speed_not_active;
    public Sprite but_firespeed_active, but_firespeed_not_active;
    public Sprite but_damage_active, but_damage_not_active;
    public Sprite but_money_active, but_money_not_active;

    private int lvl_speed, lvl_fire_speed, lvl_damage, lvl_money;
    public TMP_Text t_lvl_speed, t_lvl_fire_speed, t_lvl_damage, t_lvl_money;

    public float speed_price, fire_speed_price, damage_price, money_price;
    public TMP_Text t_speed_price, t_fire_speed_price, t_damage_price, t_money_price;

    public float speed_price_ruby, fire_speed_price_ruby, damage_price_ruby, money_price_ruby;
    public TMP_Text t_speed_price_ruby, t_fire_speed_price_ruby, t_damage_price_ruby, t_money_price_ruby;

    private bool upg_panel_active;

    public Sprite down_but_upgrade_active, down_but_upgrade_not_active, down_but_world_active, down_but_world_not_active;


    private void Start()
    {
        but_speed.GetComponent<Animator>().SetBool("enter", true);
        upg_panel_active = false;
        but_exit.SetActive(false);

        panel_speed.SetActive(false);
        panel_fire_speed.SetActive(false);
        panel_damage.SetActive(false);
        panel_money.SetActive(false);

        but_speed.sprite = but_speed_active;
        but_fire_speed.sprite = but_firespeed_not_active;
        but_damage.sprite = but_damage_not_active;
        but_money.sprite = but_money_not_active;

        if (!PlayerPrefs.HasKey("lvl_speed"))
            lvl_speed = 1;
        else lvl_speed = PlayerPrefs.GetInt("lvl_speed");

        if (!PlayerPrefs.HasKey("lvl_fire_speed"))
            lvl_fire_speed = 1;
        else lvl_fire_speed = PlayerPrefs.GetInt("lvl_fire_speed");

        if (!PlayerPrefs.HasKey("lvl_damage"))
            lvl_damage = 1;
        else lvl_damage = PlayerPrefs.GetInt("lvl_damage");

        if (!PlayerPrefs.HasKey("lvl_money"))
            lvl_money = 1;
        else lvl_money = PlayerPrefs.GetInt("lvl_money");

        if (!PlayerPrefs.HasKey("speed_price"))
            speed_price = 10;
        else speed_price = PlayerPrefs.GetFloat("speed_price");

        if (!PlayerPrefs.HasKey("fire_speed_price"))
            fire_speed_price = 10;
        else fire_speed_price = PlayerPrefs.GetFloat("fire_speed_price");

        if (!PlayerPrefs.HasKey("damage_price"))
            damage_price = 10;
        else damage_price = PlayerPrefs.GetFloat("damage_price");

        if (!PlayerPrefs.HasKey("money_price"))
            money_price = 10;
        else money_price = PlayerPrefs.GetFloat("money_price");

        if (!PlayerPrefs.HasKey("speed_price_ruby"))
            speed_price_ruby = 2;
        else speed_price_ruby = PlayerPrefs.GetFloat("speed_price_ruby");

        if (!PlayerPrefs.HasKey("fire_speed_price_ruby"))
            fire_speed_price_ruby = 2;
        else fire_speed_price_ruby = PlayerPrefs.GetFloat("fire_speed_price_ruby");

        if (!PlayerPrefs.HasKey("damage_price_ruby"))
            damage_price_ruby = 2;
        else damage_price_ruby = PlayerPrefs.GetFloat("damage_price_ruby");

        if (!PlayerPrefs.HasKey("money_price_ruby"))
            money_price_ruby = 2;
        else money_price_ruby = PlayerPrefs.GetFloat("money_price_ruby");

        if (!PlayerPrefs.HasKey("speed_count"))
            speed_count = 55;
        else speed_count = PlayerPrefs.GetFloat("speed_count");

        if (!PlayerPrefs.HasKey("fire_speed_count"))
            fire_speed_count = 0.2f;
        else fire_speed_count = PlayerPrefs.GetFloat("fire_speed_count");

        if (!PlayerPrefs.HasKey("damage_count"))
            damage_count = 1;
        else damage_count = PlayerPrefs.GetFloat("damage_count");

        if (!PlayerPrefs.HasKey("money_count"))
            money_count = 1;
        else money_count = PlayerPrefs.GetFloat("money_count");

        if (!PlayerPrefs.HasKey("coronavirus_count"))
            coronavirus_count = 1;
        else coronavirus_count = PlayerPrefs.GetFloat("coronavirus_count");
    }

    void Update()
    {
        t_lvl_speed.text = "Уровень - " + lvl_speed;
        t_lvl_fire_speed.text = "Уровень - " + lvl_fire_speed;
        t_lvl_damage.text = "Уровень - " + lvl_damage;
        t_lvl_money.text = "Уровень - " + lvl_money;

        t_speed_price.text = "" + speed_price;
        t_fire_speed_price.text = "" + fire_speed_price;
        t_damage_price.text = "" + damage_price;
        t_money_price.text = "" + money_price;
        
        t_speed_price_ruby.text = "" + speed_price_ruby;
        t_fire_speed_price_ruby.text = "" + fire_speed_price_ruby;
        t_damage_price_ruby.text = "" + damage_price_ruby;
        t_money_price_ruby.text = "" + money_price_ruby;
    }

    void Speed_Price_Up()
    {
        if (lvl_speed <= 3)
        {
            speed_price += 5;
        }

        if (lvl_speed > 3 && lvl_speed <= 5)
        {
            speed_price += 10;
        }

        if (lvl_speed > 5 && lvl_speed <= 8)
        {
            speed_price += 15;
        }

        if (lvl_speed > 8 && lvl_speed <= 12)
        {
            speed_price += 25;
        }

        if (lvl_speed > 12 && lvl_speed <= 18)
        {
            speed_price += 40;
        }

        if (lvl_speed > 18 && lvl_speed <= 25)
        {
            speed_price += 60;
        }

        if (lvl_speed > 25 && lvl_speed <= 30)
        {
            speed_price += 80;
        }

        if (lvl_speed > 30)
        {
            speed_price += 100;
        }

        PlayerPrefs.SetFloat("speed_price", speed_price);
    }

    void Fire_Speed_Price_Up()
    {
        if (lvl_fire_speed <= 3)
        {
            fire_speed_price += 5;
        }

        if (lvl_fire_speed > 3 && lvl_fire_speed <= 5)
        {
            fire_speed_price += 10;
        }

        if (lvl_fire_speed > 5 && lvl_fire_speed <= 8)
        {
            fire_speed_price += 15;
        }

        if (lvl_fire_speed > 8 && lvl_fire_speed <= 12)
        {
            fire_speed_price += 25;
        }

        if (lvl_fire_speed > 12 && lvl_fire_speed <= 18)
        {
            fire_speed_price += 40;
        }

        if (lvl_fire_speed > 18 && lvl_fire_speed <= 25)
        {
            fire_speed_price += 60;
        }

        if (lvl_fire_speed > 25 && lvl_fire_speed <= 30)
        {
            fire_speed_price += 80;
        }

        if (lvl_fire_speed > 30)
        {
            fire_speed_price += 100;
        }

        PlayerPrefs.SetFloat("fire_speed_price", fire_speed_price);
    }

    void Damage_Price_Up()
    {
        if (lvl_damage <= 3)
        {
            damage_price += 5;
        }

        if (lvl_damage > 3 && lvl_damage <= 5)
        {
            damage_price += 10;
        }

        if (lvl_damage > 5 && lvl_damage <= 8)
        {
            damage_price += 15;
        }

        if (lvl_damage > 8 && lvl_damage <= 12)
        {
            damage_price += 25;
        }

        if (lvl_damage > 12 && lvl_damage <= 18)
        {
            damage_price += 40;
        }

        if (lvl_damage > 18 && lvl_damage <= 25)
        {
            damage_price += 60;
        }

        if (lvl_damage > 25 && lvl_damage <= 30)
        {
            damage_price += 80;
        }

        if (lvl_damage > 30)
        {
            damage_price += 100;
        }

        PlayerPrefs.SetFloat("damage_price", damage_price);
    }

    void Money_Price_Up()
    {
        if (lvl_money <= 3)
        {
            money_price += 5;
        }

        if (lvl_money > 3 && lvl_money <= 5)
        {
            money_price += 10;
        }

        if (lvl_money > 5 && lvl_money <= 8)
        {
            money_price += 15;
        }

        if (lvl_money > 8 && lvl_money <= 12)
        {
            money_price += 25;
        }

        if (lvl_money > 12 && lvl_money <= 18)
        {
            money_price += 40;
        }

        if (lvl_money > 18 && lvl_money <= 25)
        {
            money_price += 60;
        }

        if (lvl_money > 25 && lvl_money <= 30)
        {
            money_price += 80;
        }

        if (lvl_money > 30)
        {
            money_price += 100;
        }

        PlayerPrefs.SetFloat("money_price", money_price);
    }

    public void But_Upgrade_Speed()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().money >= speed_price)
        {
            switch (lvl_speed)
            {
                case 1:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 55f;
                    break;
                case 2:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 57f;
                    break;
                case 3:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 59f;
                    break;
                case 4:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 62f;
                    break;
                case 5:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 64f;
                    break;
                case 6:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 66f;
                    break;
                case 7:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 68f;
                    break;
                case 8:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 69f;
                    break;
                case 9:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 71f;
                    break;
                case 10:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                    lvl_speed++;
                    speed_count = 53f;
                    break;
            }

            if (lvl_speed > 10)
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().money -= speed_price;
                lvl_speed++;
                speed_count += 0.5f;
            }

            PlayerPrefs.SetInt("lvl_speed", lvl_speed);
            PlayerPrefs.SetFloat("speed_count", speed_count);
            PlayerPrefs.SetFloat("money", GameObject.Find("GameManager").GetComponent<LevelController>().money);

            Speed_Price_Up();
        }
    }

    public void But_Upgrade_FireSpeed()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().money >= fire_speed_price)
        {
            switch (lvl_fire_speed)
            {
                case 1:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.2f;
                    break;
                case 2:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.19f;
                    break;
                case 3:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.185f;
                    break;
                case 4:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.175f;
                    break;
                case 5:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.17f;
                    break;
                case 6:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.165f;
                    break;
                case 7:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.16f;
                    break;
                case 8:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.15f;
                    break;
                case 9:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.145f;
                    break;
                case 10:
                    GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                    lvl_fire_speed++;
                    fire_speed_count = 0.135f;
                    break;
            }

            if (lvl_fire_speed > 10)
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().money -= fire_speed_price;
                lvl_fire_speed++;
                speed_count -= 0.05f;
            }

            PlayerPrefs.SetInt("lvl_fire_speed", lvl_fire_speed);
            PlayerPrefs.SetFloat("fire_speed_count", fire_speed_count);
            PlayerPrefs.SetFloat("money", GameObject.Find("GameManager").GetComponent<LevelController>().money);

            Fire_Speed_Price_Up();
        }
    }

    public void But_Upgrade_Damage()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().money >= damage_price)
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().money -= damage_price;
            lvl_damage++;
            DamageCountController();

            PlayerPrefs.SetInt("lvl_damage", lvl_damage);
            PlayerPrefs.SetFloat("money", GameObject.Find("GameManager").GetComponent<LevelController>().money);

            Damage_Price_Up();
        }
    }

    public void But_Upgrade_Money()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().money >= money_price)
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().money -= money_price;
            lvl_money++;
            money_count++;

            PlayerPrefs.SetInt("lvl_money", lvl_money);
            PlayerPrefs.SetFloat("money_count", money_count);
            PlayerPrefs.SetFloat("money", GameObject.Find("GameManager").GetComponent<LevelController>().money);

            Money_Price_Up();
        }
    }

    #region Кнопки переключения панелей
    public void But_Speed()
    {
        panel_speed.SetActive(true);
        panel_fire_speed.SetActive(false);
        panel_damage.SetActive(false);
        panel_money.SetActive(false);

        but_speed.sprite = but_speed_active;
        but_fire_speed.sprite = but_firespeed_not_active;
        but_damage.sprite = but_damage_not_active;
        but_money.sprite = but_money_not_active;

        but_speed.GetComponent<Animator>().SetBool("tap", true);
        StartCoroutine(OffAnim());
    }

    public void But_FireSpeed()
    {
        panel_speed.SetActive(false);
        panel_fire_speed.SetActive(true);
        panel_damage.SetActive(false);
        panel_money.SetActive(false);

        but_speed.sprite = but_speed_not_active;
        but_fire_speed.sprite = but_firespeed_active;
        but_damage.sprite = but_damage_not_active;
        but_money.sprite = but_money_not_active;

        but_fire_speed.GetComponent<Animator>().SetBool("tap", true);
        StartCoroutine(OffAnim());
    }

    public void But_Damage()
    {
        panel_speed.SetActive(false);
        panel_fire_speed.SetActive(false);
        panel_damage.SetActive(true);
        panel_money.SetActive(false);

        but_speed.sprite = but_speed_not_active;
        but_fire_speed.sprite = but_firespeed_not_active;
        but_damage.sprite = but_damage_active;
        but_money.sprite = but_money_not_active;

        but_damage.GetComponent<Animator>().SetBool("tap", true);
        StartCoroutine(OffAnim());
    }

    public void But_Money()
    {
        panel_speed.SetActive(false);
        panel_fire_speed.SetActive(false);
        panel_damage.SetActive(false);
        panel_money.SetActive(true);

        but_speed.sprite = but_speed_not_active;
        but_fire_speed.sprite = but_firespeed_not_active;
        but_damage.sprite = but_damage_not_active;
        but_money.sprite = but_money_active;

        but_money.GetComponent<Animator>().SetBool("tap", true);
        StartCoroutine(OffAnim());
    }

    IEnumerator OffAnim()
    {
        yield return new WaitForSeconds(0.1f);
        but_speed.GetComponent<Animator>().SetBool("tap", false);
        but_fire_speed.GetComponent<Animator>().SetBool("tap", false);
        but_damage.GetComponent<Animator>().SetBool("tap", false);
        but_money.GetComponent<Animator>().SetBool("tap", false);
    }

    public void But_Upgrade_Panel()
    {
        if (!upg_panel_active)
        {
            StartCoroutine(Upg_Panle_Anim_Enter());
            but_exit.SetActive(true);
            return;
        }

        if (upg_panel_active)
        {
            StartCoroutine(Upg_Panle_Anim_Exit());
            but_exit.SetActive(false);
            return;
        }
    }

    IEnumerator Upg_Panle_Anim_Enter()
    {
        but_speed.sprite = but_speed_active;
        but_fire_speed.sprite = but_firespeed_not_active;
        but_damage.sprite = but_damage_not_active;
        but_money.sprite = but_money_not_active;
        panel_speed.SetActive(true);

        yield return new WaitForSeconds(0.15f);

        buts_chose.SetActive(true);
        upg_panel_active = !upg_panel_active;
    }

    IEnumerator Upg_Panle_Anim_Exit()
    {
        buts_chose.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(0.12f);

        buts_chose.SetActive(false);
        panel_speed.GetComponent<Animator>().SetBool("exit", true);
        panel_fire_speed.GetComponent<Animator>().SetBool("exit", true);
        panel_damage.GetComponent<Animator>().SetBool("exit", true);
        panel_money.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(0.13f);

        panel_speed.SetActive(false);
        panel_fire_speed.SetActive(false);
        panel_damage.SetActive(false);
        panel_money.SetActive(false);

        upg_panel_active = !upg_panel_active;
    }
    #endregion

    public void CoronavirusCountController()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 5)
            coronavirus_count += 4;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 5 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 10)
            coronavirus_count += 7;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 10 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 15)
            coronavirus_count += 20;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 15 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 20)
            coronavirus_count += 50;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 20 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 25)
            coronavirus_count += 100;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 25 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 30)
            coronavirus_count += 165;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 30 && GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 80)
            coronavirus_count += 165;

        if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 80)
            coronavirus_count += 100;

        PlayerPrefs.SetFloat("coronavirus_count", coronavirus_count);
    }

    public void DamageCountController()
    {
        if (lvl_damage <= 10)
            damage_count += 1;

        if (lvl_damage > 10 && lvl_damage <= 15)
            damage_count += 3;

        if (lvl_damage > 15 && lvl_damage <= 20)
            damage_count += 7;

        if (lvl_damage > 20 && lvl_damage <= 25)
            damage_count += 13;

        if (lvl_damage > 25 && lvl_damage <= 30)
            damage_count += 15;

        if (lvl_damage > 30 && lvl_damage <= 60)
            damage_count += 18;

        if (lvl_damage > 60)
            damage_count += 20;

        PlayerPrefs.SetFloat("damage_count", damage_count);
    }

    //РУБИНЫ
    public void But_Upgrade_Speed_RUBY()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().ruby >= speed_price_ruby)
        {
            switch (lvl_speed)
            {
                case 1:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 55f;
                    break;
                case 2:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 57f;
                    break;
                case 3:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 59f;
                    break;
                case 4:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 62f;
                    break;
                case 5:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 64f;
                    break;
                case 6:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 66f;
                    break;
                case 7:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 68f;
                    break;
                case 8:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 69f;
                    break;
                case 9:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 71f;
                    break;
                case 10:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                    lvl_speed++;
                    speed_count = 53f;
                    break;
            }

            if (lvl_speed > 10)
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= speed_price_ruby;
                lvl_speed++;
                speed_count += 0.5f;
            }

            PlayerPrefs.SetInt("lvl_speed", lvl_speed);
            PlayerPrefs.SetFloat("speed_count", speed_count);
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);

            speed_price_ruby++;

            PlayerPrefs.SetFloat("speed_price_ruby", speed_price_ruby);
        }
    }

    public void But_Upgrade_FireSpeed_RUBY()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().ruby >= fire_speed_price_ruby)
        {
            switch (lvl_fire_speed)
            {
                case 1:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.2f;
                    break;
                case 2:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.19f;
                    break;
                case 3:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.185f;
                    break;
                case 4:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.175f;
                    break;
                case 5:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.17f;
                    break;
                case 6:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.165f;
                    break;
                case 7:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.16f;
                    break;
                case 8:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.15f;
                    break;
                case 9:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.145f;
                    break;
                case 10:
                    GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                    lvl_fire_speed++;
                    fire_speed_count = 0.135f;
                    break;
            }

            if (lvl_fire_speed > 10)
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= fire_speed_price_ruby;
                lvl_fire_speed++;
                speed_count -= 0.05f;
            }

            PlayerPrefs.SetInt("lvl_fire_speed", lvl_fire_speed);
            PlayerPrefs.SetFloat("fire_speed_count", fire_speed_count);
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);

            fire_speed_price_ruby++;

            PlayerPrefs.SetFloat("fire_speed_price_ruby", fire_speed_price_ruby);
        }
    }

    public void But_Upgrade_Damage_RUBY()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().ruby >= damage_price_ruby)
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= damage_price_ruby;
            lvl_damage++;
            DamageCountController();

            damage_price_ruby++;

            PlayerPrefs.SetInt("lvl_damage", lvl_damage);
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
            PlayerPrefs.SetFloat("damage_price_ruby", damage_price_ruby);
        }
    }

    public void But_Upgrade_Money_RUBY()
    {
        if (GameObject.Find("GameManager").GetComponent<LevelController>().ruby >= money_price_ruby)
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby -= money_price_ruby;
            lvl_money++;
            money_count++;

            money_price_ruby++;

            PlayerPrefs.SetInt("lvl_money", lvl_money);
            PlayerPrefs.SetFloat("money_count", money_count);
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
            PlayerPrefs.SetFloat("money_price_ruby", money_price_ruby);
        }
    }
}
