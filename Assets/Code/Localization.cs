﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Localization : MonoBehaviour
{
    public GameObject pop_up;
    public GameObject pop_up_sub;

    public Image but_lz;
    public Sprite but_ru, but_eng;

    public static string curr_language;

    //Тексты
    public TMP_Text store;
    public TMP_Text click_to_play;
    public TMP_Text soon;
    public TMP_Text speed;
    public TMP_Text power;
    public TMP_Text damage;
    public TMP_Text coins;
    public TMP_Text _continue;
    public TMP_Text language;
    public TMP_Text choose;
    public TMP_Text actively;
    public TMP_Text sub_1, sub_2, sub_3, sub_4, sub_5;

    private void Start()
    {
        if (!PlayerPrefs.HasKey("chose_language"))
        {
            pop_up.SetActive(true);
            curr_language = "eng";
            Chose_ENG();
            PlayerPrefs.SetInt("chose_language", 1);
            PlayerPrefs.SetString("language", "eng");
        }
        else pop_up_sub.SetActive(true);

        curr_language = PlayerPrefs.GetString("language");
        if (curr_language == "ru") Chose_RU();
        if (curr_language == "eng") Chose_ENG();
    }

    public void Chose_RU()
    {
        curr_language = "ru";
        store.text = "Магазин";
        click_to_play.text = "Нажми чтобы играть";
        soon.text = "Скоро";
        speed.text = "Скорость";
        power.text = "Мощность";
        damage.text = "Урон";
        coins.text = "Монеты";
        _continue.text = "Продолжить?";
        language.text = "Язык";
        choose.text = "Выбрать";
        actively.text = "Активно";
        sub_1.text = "ПОЛНЫЙ ДОСТУП";
        sub_2.text = "ВСЕ СТРАНЫ\nВСЕ КОРАБЛИ";
        sub_3.text = "5 сердец каждый день\n10 рубинов каждый день\nМонеты Х5";
        sub_4.text = "Начать бесплатно";
        sub_5.text = "3 дня бесплатно затем 299р в неделю";

        but_lz.sprite = but_ru;

        PlayerPrefs.SetString("language", "ru");
    }

    public void Chose_ENG()
    {
        curr_language = "eng";
        store.text = "Shop";
        click_to_play.text = "Tap to play";
        soon.text = "Soon";
        speed.text = "Speed";
        power.text = "Power";
        damage.text = "Damage";
        coins.text = "Coins";
        _continue.text = "Continue?";
        language.text = "Language";
        choose.text = "Choose";
        actively.text = "Actively";
        sub_1.text = "FULL ACCESS";
        sub_2.text = "ALL COUNTRIES\nALL SHIPS";
        sub_3.text = "5 hearts every day\n10 rubies every day\nCoins X5";
        sub_4.text = "Start for free";
        sub_5.text = "3 days for free then 3.99$ per week";

        but_lz.sprite = but_eng;

        PlayerPrefs.SetString("language", "eng");
    }

    public void PopUp_On()
    {
        pop_up.SetActive(true);
        pop_up.GetComponent<Animator>().SetBool("exit", false);
    }

    public void PopUp_Off()
    {
        StartCoroutine(ClosePopUp());
    }

    IEnumerator ClosePopUp()
    {
        pop_up.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(1f);

        pop_up.SetActive(false);
    }
}
