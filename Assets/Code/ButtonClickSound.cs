﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonClickSound : MonoBehaviour
{
    AudioSource aud;
    public AudioClip aud_but_click, aud_shoot, aud_boom;

    public GameObject coronavirus_aud, player_aud;

    private void Start()
    {
        aud = GetComponent<AudioSource>();
    }

    public void ClickPlay()
    {
        aud.clip = aud_but_click;
        aud.Play();
    }

    public void PlayerPlay()
    {
        if (!player_aud.GetComponent<AudioSource>().isPlaying)
        {
            player_aud.GetComponent<AudioSource>().clip = aud_shoot;
            player_aud.GetComponent<AudioSource>().Play();
        }
    }

    public void CoronavirusPlay()
    {
        coronavirus_aud.GetComponent<AudioSource>().clip = aud_boom;
        coronavirus_aud.GetComponent<AudioSource>().Play();
    }
}
