﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Android;

public class PlayerController : MonoBehaviour
{
    Vector2 mouse;
    private float x, y;

    public Transform bullet_pos;
    public GameObject bullet;

    //Stats
    public float bullet_speed;
    public float bullet_dmg;
    public float fire_speed;

    public bool press;

    private float width, height;
    private Vector3 position;

    public GameObject popUp_revive;

    public bool not_death; //Неуязвимость

    SpriteRenderer spr;
    private bool fade;


    private void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        fade = false;

        not_death = false;
        press = true;

        width = (float)Screen.width / 2f;
        height = (float)Screen.height / 2f;
    }

    private void Update()
    {
        mouse = Input.mousePosition;
        mouse = Camera.main.ScreenToWorldPoint(mouse);

        if (GameObject.Find("GameManager").GetComponent<LevelController>().move_accept)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);

                if (touch.phase == TouchPhase.Moved)
                {
                    Vector2 pos = touch.position;
                    pos.x = (pos.x - width) / width;
                    pos.y = (pos.y = height) / height;
                    position = new Vector3(-pos.x, pos.y, 0);

                    if (press)
                    {
                        x = transform.position.x - pos.x;
                        y = transform.position.y - pos.y;

                        transform.position = new Vector2(pos.x + x, pos.y + y);
                        
                        press = false;
                    }

                    transform.position = new Vector2(pos.x + x, pos.y + y);
                }
            }

            if (Input.GetMouseButton(0))
            {
                if (press)
                {
                    x = transform.position.x - mouse.x;
                    y = transform.position.y - mouse.y;

                    transform.position = new Vector2(mouse.x + x, mouse.y + y); 
                    press = false;
                }

                if (!press)
                    transform.position = new Vector2(mouse.x + x, mouse.y + y);
            }

            if (Input.GetMouseButtonUp(0))
            {
                press = true;
            }
        }

        if (transform.position.x < -14.7f)
        {
            transform.position = new Vector2(-14.7f, transform.position.y);
            x = transform.position.x - mouse.x;
            y = transform.position.y - mouse.y;
        }

        if (transform.position.x > 14.7f)
        {
            transform.position = new Vector2(14.7f, transform.position.y);
            x = transform.position.x - mouse.x;
            y = transform.position.y - mouse.y;
        }

        if (transform.position.y < -30.8f)
        {
            transform.position = new Vector2(transform.position.x, -30.8f);
            x = transform.position.x - mouse.x;
            y = transform.position.y - mouse.y;
        }

        if (transform.position.y > 30.8f)
        {
            transform.position = new Vector2(transform.position.x, 30.8f);
            x = transform.position.x - mouse.x;
            y = transform.position.y - mouse.y;
        }

        bullet_speed = GameObject.Find("GameManager").GetComponent<Upgrader>().speed_count;
        bullet_dmg = GameObject.Find("GameManager").GetComponent<Upgrader>().damage_count;
        fire_speed = GameObject.Find("GameManager").GetComponent<Upgrader>().fire_speed_count;

        if (not_death)
        {
            FadeInOut();
        }
    }

    IEnumerator Shoot()
    {
        yield return new WaitForSeconds(fire_speed);
        if (GameObject.Find("GameManager").GetComponent<LevelController>().move_accept)
        {
            //GameObject.Find("AudioController").GetComponent<ButtonClickSound>().PlayerPlay();
            GameObject bul = Instantiate(bullet, bullet_pos.position, transform.rotation);
        
            bul.GetComponent<Bullet>().bullet_dmg = bullet_dmg;
            bul.GetComponent<Bullet>().bullet_speed = bullet_speed;

            StartCoroutine(Shoot());
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!not_death)
        {
            if (col.tag == "enemy")
            {
                GameObject.Find("GameManager").GetComponent<LevelController>().move_accept = false;
                popUp_revive.SetActive(true);
                popUp_revive.GetComponent<PopUp_Revive>().StartPopUp();
            }
        }
    }

    public void NotDeathTimer()
    {
        StartCoroutine(NotDeathEnum());
    }

    IEnumerator NotDeathEnum()
    {
        not_death = true;
        yield return new WaitForSeconds(2);
        not_death = false;
        Color c = spr.material.color;
        c.a = 1;
        spr.material.color = c;
    }

    void FadeInOut()
    {
        Color c = spr.material.color;

        if (fade)
        {
            c.a -= Time.deltaTime * 3;
            spr.material.color = c;

            if (c.a <= 0.2f)
                fade = false;
        }

        if (!fade)
        {
            c.a += Time.deltaTime * 3;
            spr.material.color = c;

            if (c.a >= 1f)
                fade = true;
        }
    }

    public void StartShoot()
    {
        StartCoroutine(Shoot());
    }
}
