﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    public float shakeAmount;
    private bool active;


    private void Start()
    {
        active = false;
    }

    public void ShakeCamera(float shalePwr)
    {
        shakeAmount = shalePwr;

        StartCoroutine(Shake());
    }

    private void Update()
    {
        if (active)
        {
            Debug.Log("Shaking");
            Vector2 ShakePos = Random.insideUnitCircle * shakeAmount;

            transform.position = new Vector3(transform.position.x + ShakePos.x, transform.position.y + ShakePos.y, transform.position.z);
        }
    }

    IEnumerator Shake()
    {
        active = true;

        yield return new WaitForSeconds(0.2f);

        active = false;
        transform.position = new Vector3(0, 0, transform.position.z);
    }
}
