﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

public class PopUp_Shop : MonoBehaviour
{
    public GameObject pop_up;
    private bool ads_active;

    private void Start()
    {
        ads_active = false;

        pop_up.SetActive(false);

        if (Monetization.isSupported) Monetization.Initialize("3513712", false);
    }

    public void But_Ruby_Ads()
    {
        if (Monetization.IsReady("rewardedVideo"))
        {
            ads_active = true;
            ShowAdCallbacks options = new ShowAdCallbacks();
            options.finishCallback = Ruby_Ads_Reward;
            ShowAdPlacementContent ad = Monetization.GetPlacementContent("rewardedVideo") as ShowAdPlacementContent;
            ad.Show(options);
        }
    }

    private void Ruby_Ads_Reward(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            ads_active = false;
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 2;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        } else if (result == ShowResult.Skipped)
        {
            ads_active = false;
        } else if (result == ShowResult.Failed)
        {
            ads_active = false;
        }
    }

    public void PopUp_On()
    {
        pop_up.SetActive(true);
        pop_up.GetComponent<Animator>().SetBool("exit", false);
    }

    public void PopUp_Off()
    {
        if (!ads_active)
            StartCoroutine(ClosePopUp());
    }

    IEnumerator ClosePopUp()
    {
        pop_up.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(1f);

        pop_up.SetActive(false);
    }
}
