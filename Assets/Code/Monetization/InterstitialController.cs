﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Monetization;

public class InterstitialController : MonoBehaviour
{
    private int win_count;

    private void Start()
    {
        if (Monetization.isSupported) Monetization.Initialize("3513712", false);

        win_count = 0;
    }

    public void InterstitialPlay()
    {
        if (PlayerPrefs.GetInt("ads_off") != 1)
        {
            if (Monetization.IsReady("video"))
            {
                ShowAdCallbacks options = new ShowAdCallbacks();
                options.finishCallback = Interstitial;
                ShowAdPlacementContent ad = Monetization.GetPlacementContent("video") as ShowAdPlacementContent;
                ad.Show(options);
            }
        }
    }

    private void Interstitial(ShowResult result)
    {
        if (result == ShowResult.Finished)
        {
            //
        }
        else if (result == ShowResult.Skipped)
        {
            //
        }
        else if (result == ShowResult.Failed)
        {
            //
        }
    }

    public void Inter_But_Exit()
    {
        InterstitialPlay();
    }

    public void Inter_Win_Lose()
    {
        win_count++;

        if (win_count >= 2)
        {
            InterstitialPlay();
            win_count = 0;
        }
    }
}
