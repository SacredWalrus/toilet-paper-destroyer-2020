﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPCheck : MonoBehaviour
{
    public static bool ads_off;


    private void Awake()
    {
        if (!PlayerPrefs.HasKey("ads_off"))
            ads_off = false;
        else PlayerPrefs.SetInt("ads_off", 0);  //0 - реклама работает, 1 - реклама отключена

        IAPController.OnPurchaseNonConsumable += IAPController_OnPurchaseNonConsumable;
        IAPController.OnPurchaseConsumable += IAPController_OnPurchaseConsumable;
        IAPController.OnPurchaseSubscription += IAPController_OnPurchaseSubscription;

        Check_Buying();
    }

    private void Check_Buying()
    {
        if (IAPController.CheckBuyState("ads_off"))
        {
            ads_off = true;
            PlayerPrefs.SetInt("ads_off", 1);
        }

        if (IAPController.CheckBuyState("all_unlock"))
        {
            LevelController.subs_active = true;
            PlayerPrefs.SetInt("subs_active", 1);
        }
    }

    private void IAPController_OnPurchaseSubscription(PurchaseEventArgs args)
    {
        LevelController.subs_active = true;
        PlayerPrefs.SetInt("subs_active", 1);
    }

    private void IAPController_OnPurchaseNonConsumable(PurchaseEventArgs args)
    {
        ads_off = true;
        PlayerPrefs.SetInt("ads_off", 1);
    }

    private void IAPController_OnPurchaseConsumable(PurchaseEventArgs args)
    {
        if (args.purchasedProduct.definition.id == "heart_10")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().heart += 10;
            PlayerPrefs.SetFloat("heart", GameObject.Find("GameManager").GetComponent<LevelController>().heart);
        }

        if (args.purchasedProduct.definition.id == "heart_25")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().heart += 25;
            PlayerPrefs.SetFloat("heart", GameObject.Find("GameManager").GetComponent<LevelController>().heart);
        }

        if (args.purchasedProduct.definition.id == "heart_60")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().heart += 60;
            PlayerPrefs.SetFloat("heart", GameObject.Find("GameManager").GetComponent<LevelController>().heart);
        }

        if (args.purchasedProduct.definition.id == "ruby_40")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 40;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        }

        if (args.purchasedProduct.definition.id == "ruby_100")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 100;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        }

        if (args.purchasedProduct.definition.id == "ruby_240")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 240;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        }

        if (args.purchasedProduct.definition.id == "ruby_800")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 800;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        }

        if (args.purchasedProduct.definition.id == "ruby_2500")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().ruby += 2500;
            PlayerPrefs.SetFloat("ruby", GameObject.Find("GameManager").GetComponent<LevelController>().ruby);
        }
    }

    private void Price()
    {
        //string s = args.purchasedProduct.metadata.localizedPrice;
    }
}
