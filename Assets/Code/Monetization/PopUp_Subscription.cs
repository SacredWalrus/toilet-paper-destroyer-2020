﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUp_Subscription : MonoBehaviour
{
    public GameObject pop_up;

    private void Start()
    {
        //pop_up.SetActive(false);
    }

    public void PopUp_On()
    {
        pop_up.SetActive(true);
        pop_up.GetComponent<Animator>().SetBool("exit", false);
    }

    public void PopUp_Off()
    {
        StartCoroutine(ClosePopUp());
    }

    IEnumerator ClosePopUp()
    {
        pop_up.GetComponent<Animator>().SetBool("exit", true);

        yield return new WaitForSeconds(1f);

        pop_up.SetActive(false);
    }
}
