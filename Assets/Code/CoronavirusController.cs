﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class CoronavirusController : MonoBehaviour
{
    public bool up_coronavirus;  //Если true значит этот коронавирус может биться ою потолок

    public enum Size { BIG, MEDIUM, SMALL };
    public Size size;
    public List<GameObject> cor_medium, cor_small;

    public List<Sprite> spr_mass;

    public float start_hp;  //Запоминаем объем чтобы прибавить его к счету
    public float hp;
    public float speedUD, speedLR;
    public float rotateSpeed;
    public float gravity_mass;

    public int dirUD, dirLR;  //1 - Down/Left, 2 - Up/Right

    public GameObject money_obj;

    public TMP_Text hp_text;
    public GameObject UI;

    Rigidbody2D rb2d;
    SpriteRenderer spr;

    public bool move_accept;
    private float coronavirus_count;

    private bool col_off;  //Отключаю столкновение со стенами чтобы не делать тряску

    private bool fade_out;

    public GameObject boom_green, boom_red, boom_yellow;
    public GameObject boom;


    private void Awake()
    {
        spr = GetComponent<SpriteRenderer>();

        if (hp < 1) hp = 1;
        col_off = false;

        Color c = spr.material.color;
        c.a = 1f;
        spr.material.color = c;

        fade_out = false;
    }

    private void Start()
    {
        coronavirus_count = GameObject.Find("GameManager").GetComponent<Upgrader>().coronavirus_count;
        move_accept = true;
        hp_text.transform.position = new Vector3(hp_text.transform.position.x, hp_text.transform.position.y, transform.position.z);
        rb2d = GetComponent<Rigidbody2D>();

        int r = Random.Range(0, spr_mass.Count);
        spr.sprite = spr_mass[r];
        if (r == 0)
        {
            boom = boom_green;
        }
        if (r == 1)
        {
            boom = boom_red;
        }
        if (r == 2)
        {
            boom = boom_yellow;
        }

        gravity_mass = 0;
        speedLR = Random.Range(5, 15);
        rotateSpeed = Random.Range(-20, 20);

        if (!up_coronavirus)
        {
            dirLR = Random.Range(1, 3);

            if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level <= 5)
                hp = (int)Random.Range(1, coronavirus_count + (coronavirus_count * 0.1f));

            if (GameObject.Find("GameManager").GetComponent<LevelController>().curr_level > 5)
                hp = (int)Random.Range(coronavirus_count - (coronavirus_count * 0.2f), coronavirus_count + (coronavirus_count * 0.2f));
        }

        if (size == Size.BIG)
        {
            speedUD = Random.Range(5.1f, 10.1f);
        }

        if (size == Size.MEDIUM)
        {
            speedUD = Random.Range(7.1f, 17.1f);
        }

        if (size == Size.SMALL)
        {
            speedUD = Random.Range(15.1f, 20.1f);
        }

        start_hp = hp;

        //В какую сторону
        int dir = Random.Range(1, 3);
    }

    void Controller()
    {
        //transform.Rotate(new Vector3(0, 0, rotateSpeed));

        if (dirUD == 1)
        {
            transform.Translate(new Vector2(0, -speedUD * Time.deltaTime));
        }

        if (dirUD == 2)
        {
            transform.Translate(new Vector2(0, speedUD * Time.deltaTime));
        }

        if (dirLR == 1)
        {
            transform.Translate(new Vector2(-speedLR * Time.deltaTime, 0));
        }

        if (dirLR == 2)
        {
            transform.Translate(new Vector2(speedLR * Time.deltaTime, 0));
        }
    }

    private void Update()
    {
        if (fade_out)
        {
            Color c = spr.material.color;
            c.a -= Time.deltaTime * 6;
            spr.material.color = c;
        }

        coronavirus_count = GameObject.Find("GameManager").GetComponent<Upgrader>().coronavirus_count;

        if (move_accept)
            Controller();

        if (hp < 1000)
            hp_text.text = hp + "";

        if (hp >= 1000 && hp < 10000)
        {
            string s1 = hp.ToString();

            hp_text.text = s1[0] + "." + s1[1] + "K";
        }

        if (hp >= 10000 && hp < 100000)
        {
            string s1 = hp.ToString();

            hp_text.text = s1[0] + s1[1] + "." + s1[2] + "K";
        }

        if (hp >= 100000 && hp < 1000000)
        {
            string s1 = hp.ToString();

            hp_text.text = s1[0] + s1[1] + s1[2] + "." + s1[3] + "K";
        }

        if (hp >= 100000 && hp < 1000000)
        {
            string s1 = hp.ToString();

            hp_text.text = s1[0] + "M";
        }

        if (transform.position.x < -14.7f)
        {
            dirLR = 2;
            StartCoroutine(OffColl());
            Debug.Log("Check");
        }

        if (transform.position.x > 14.7f)
        {
            dirLR = 1;
            StartCoroutine(OffColl());
            Debug.Log("Check");
        }
    }

    IEnumerator OffColl()
    {
        col_off = true;
        yield return new WaitForSeconds(0.5f);
        col_off = false;
    }

    IEnumerator Death()
    {
        yield return new WaitForSeconds(0.1f);
        Destroy(gameObject);
    }

    private void LateUpdate()
    {
        #region Смерть
        if (hp <= 0 && !fade_out)
        {
            GameObject.Find("AudioController").GetComponent<ButtonClickSound>().CoronavirusPlay();
            UI.SetActive(false);
            gameObject.tag = "Untagged";
            GameObject part = Instantiate(boom, transform.position, transform.rotation) as GameObject;
            Destroy(part, 0.5f);

            GameObject.Find("GameManager").GetComponent<LevelController>().new_score += (int)start_hp;
            GameObject.Find("GameManager").GetComponent<LevelController>().new_curr_enemy += 1;

            if (size == Size.BIG)
            {
                GameObject a1 = Instantiate(cor_medium[Random.Range(0, cor_medium.Count)], transform.position, transform.rotation) as GameObject;
                GameObject a2 = Instantiate(cor_medium[Random.Range(0, cor_medium.Count)], transform.position, transform.rotation) as GameObject;

            layer_rand_1:

                int layer_r_1_1 = Random.Range(0, 950);
                int layer_r_1_2 = Random.Range(0, 950);
                if (GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass != null)
                {
                    if (GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Contains(layer_r_1_1)
                        || GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Contains(layer_r_1_2))
                    {
                        goto layer_rand_1;
                    }
                }

                a1.transform.position = new Vector3(a1.transform.position.x, a1.transform.position.y, layer_r_1_1);
                a2.transform.position = new Vector3(a2.transform.position.x, a2.transform.position.y, layer_r_1_2);

                a1.GetComponent<CoronavirusController>().up_coronavirus = true;
                a1.GetComponent<CoronavirusController>().dirUD = 2;
                a1.GetComponent<CoronavirusController>().dirLR = 1;
                a2.GetComponent<CoronavirusController>().up_coronavirus = true;
                a2.GetComponent<CoronavirusController>().dirUD = 2;
                a2.GetComponent<CoronavirusController>().dirLR = 2;

                int new_hp = (int)start_hp / 2;
                if (new_hp < 1) new_hp = 1;

                a1.GetComponent<CoronavirusController>().hp = new_hp;
                a1.GetComponent<CoronavirusController>().start_hp = new_hp;

                a2.GetComponent<CoronavirusController>().hp = new_hp;
                a2.GetComponent<CoronavirusController>().start_hp = new_hp;

                GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Add(layer_r_1_1);
                GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Add(layer_r_1_2);

                GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Add(a1);
                GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Add(a2);
            }

            if (size == Size.MEDIUM)
            {
                GameObject a1 = Instantiate(cor_small[Random.Range(0, cor_medium.Count)], transform.position, transform.rotation) as GameObject;
                GameObject a2 = Instantiate(cor_small[Random.Range(0, cor_medium.Count)], transform.position, transform.rotation) as GameObject;

            layer_rand_2:

                int layer_r_1_1 = Random.Range(0, 950);
                int layer_r_1_2 = Random.Range(0, 950);
                if (GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass != null)
                {
                    if (GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Contains(layer_r_1_1)
                        || GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Contains(layer_r_1_2))
                    {
                        goto layer_rand_2;
                    }
                }

                int new_hp = (int)start_hp / 2;
                if (new_hp < 1) new_hp = 1;

                a1.GetComponent<CoronavirusController>().hp = new_hp;
                a1.GetComponent<CoronavirusController>().start_hp = new_hp;

                a2.GetComponent<CoronavirusController>().hp = new_hp;
                a2.GetComponent<CoronavirusController>().start_hp = new_hp;

                a1.transform.position = new Vector3(a1.transform.position.x, a1.transform.position.y, layer_r_1_1);
                a2.transform.position = new Vector3(a2.transform.position.x, a2.transform.position.y, layer_r_1_2);

                a1.GetComponent<CoronavirusController>().up_coronavirus = true;
                a1.GetComponent<CoronavirusController>().dirUD = 2;
                a1.GetComponent<CoronavirusController>().dirLR = 1;
                a2.GetComponent<CoronavirusController>().up_coronavirus = true;
                a2.GetComponent<CoronavirusController>().dirUD = 2;
                a2.GetComponent<CoronavirusController>().dirLR = 2;

                GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Add(layer_r_1_1);
                GameObject.Find("GameManager").GetComponent<LevelController>().layer_mass.Add(layer_r_1_2);

                //a1.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
                //a2.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);

                //a1.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 500);
                //a2.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 500);

                //a1.GetComponent<Rigidbody2D>().AddForce(Vector2.left * 400);
                //a2.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 400);

                GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Add(a1);
                GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Add(a2);
            }

            GameObject boom_inst_1 = Instantiate(money_obj, new Vector3(Random.Range(transform.position.x - 1.5f, transform.position.x + 1.5f),
                                                                      Random.Range(transform.position.y - 1.5f, transform.position.y + 1.5f)),
                                                                      transform.rotation.normalized) as GameObject;
            GameObject boom_inst_2 = Instantiate(money_obj, new Vector3(Random.Range(transform.position.x - 1.5f, transform.position.x + 1.5f),
                                                                      Random.Range(transform.position.y - 1.5f, transform.position.y + 1.5f)),
                                                                      transform.rotation.normalized) as GameObject;
            GameObject boom_inst_3 = Instantiate(money_obj, new Vector3(Random.Range(transform.position.x - 1.5f, transform.position.x + 1.5f),
                                                                      Random.Range(transform.position.y - 1.5f, transform.position.y + 1.5f)),
                                                                      transform.rotation.normalized) as GameObject;
            GameObject boom_inst_4 = Instantiate(money_obj, new Vector3(Random.Range(transform.position.x - 1.5f, transform.position.x + 1.5f),
                                                                      Random.Range(transform.position.y - 1.5f, transform.position.y + 1.5f)),
                                                                      transform.rotation.normalized) as GameObject;
            GameObject boom_inst_5 = Instantiate(money_obj, new Vector3(Random.Range(transform.position.x - 1.5f, transform.position.x + 1.5f),
                                                                      Random.Range(transform.position.y - 1.5f, transform.position.y + 1.5f)),
                                                                      transform.rotation.normalized) as GameObject;

            if (GameObject.Find("GameManager").GetComponent<WorldController>().curr_county == WorldController.Country.Russia)
            {
                boom_inst_1.transform.parent = GameObject.Find("Russia").transform;
                boom_inst_2.transform.parent = GameObject.Find("Russia").transform;
                boom_inst_3.transform.parent = GameObject.Find("Russia").transform;
                boom_inst_4.transform.parent = GameObject.Find("Russia").transform;
                boom_inst_5.transform.parent = GameObject.Find("Russia").transform;
            }

            if (GameObject.Find("GameManager").GetComponent<WorldController>().curr_county == WorldController.Country.Italy)
            {
                boom_inst_1.transform.parent = GameObject.Find("Italy").transform;
                boom_inst_2.transform.parent = GameObject.Find("Italy").transform;
                boom_inst_3.transform.parent = GameObject.Find("Italy").transform;
                boom_inst_4.transform.parent = GameObject.Find("Italy").transform;
                boom_inst_5.transform.parent = GameObject.Find("Italy").transform;
            }

            if (GameObject.Find("GameManager").GetComponent<WorldController>().curr_county == WorldController.Country.USA)
            {
                boom_inst_1.transform.parent = GameObject.Find("USA").transform;
                boom_inst_2.transform.parent = GameObject.Find("USA").transform;
                boom_inst_3.transform.parent = GameObject.Find("USA").transform;
                boom_inst_4.transform.parent = GameObject.Find("USA").transform;
                boom_inst_5.transform.parent = GameObject.Find("USA").transform;
            }

            if (GameObject.Find("GameManager").GetComponent<WorldController>().curr_county == WorldController.Country.Market)
            {
                boom_inst_1.transform.parent = GameObject.Find("Market").transform;
                boom_inst_2.transform.parent = GameObject.Find("Market").transform;
                boom_inst_3.transform.parent = GameObject.Find("Market").transform;
                boom_inst_4.transform.parent = GameObject.Find("Market").transform;
                boom_inst_5.transform.parent = GameObject.Find("Market").transform;
            }

            GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Remove(gameObject);
            fade_out = true;
            StartCoroutine(Death());
        }
        #endregion
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Bullet")
        {
            hp -= col.gameObject.GetComponent<Bullet>().bullet_dmg;
            Destroy(col.gameObject);
        }

        if (col.tag == "enemy_dead_zone")
        {
            GameObject.Find("GameManager").GetComponent<LevelController>().active_enemy.Remove(gameObject);
            if (GameObject.Find("GameManager").GetComponent<LevelController>().dont_spawn)
                GameObject.Find("GameManager").GetComponent<LevelController>().new_curr_enemy += 1;

            Destroy(gameObject);
        }

        if (col.tag == "wallU" && up_coronavirus)
        {
            if (dirLR == 1)
            {
                dirLR = 2;
            }

            if (dirLR == 2)
            {
                dirLR = 1;
            }

            dirUD = 1;
        }

        if (!col_off)
        {
            if (col.tag == "wallLR")
            {
                if (dirLR == 1)
                {
                    dirLR = 2;
                    return;
                }

                if (dirLR == 2)
                {
                    dirLR = 1;
                    return;
                }
            }
        }
    }
}
